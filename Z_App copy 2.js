import React, { useState } from 'react';
import { StyleSheet, TextInput, View,Text, Button ,ScrollView,FlatList} from 'react-native';
import GoalItem from  './components/GoalItem';
import GoalInput from './components/GoalInput';

export default function App() { // Este es un componente funcional que usa hooks; != a componente de clase

  /*enteredGoal toma entrada desde TextInput y setEnteredGoal la modifica*/
  //const [enteredGoal, setEnteredGoal] = useState('');
  /*Cuando usuario escribe un char, actualizamos state y guardamos el txt ingresado en el state */

  const [courseGoals,setCourseGoals] = useState([]);
  const [isAddMode, setIsAddMode] = useState(false); 
  /* function goalInputHandler(enteredText) {
    setEnteredGoal(enteredGoal);
  }
  Función que atiende la entrada desde TextInput en cada tecla precionada. RN se lo pasará a mi fc */
/*   const goalInputHandler = (enteredText)=> {
    setEnteredGoal(enteredText);
  } */

  /*Cuando se precione el boton, usaremos los caracteres "texto" ingresados desde TextInput con esta función*/
//  const addGoalHandler = () =>{
  // Cuando preciono boton, exe onPress tomo el valor ingresado "enteredGoal" desde onChangeText y lo paso a APP desde GoalInput 
  const addGoalHandler = (goalTitle) =>{
    /*setCourseGoals([...courseGoals,enteredGoal]); //operador de propagación spread operator ...
    cuando actualice un estado en fc de su estado anterior, puedo usar la forma de fc de esta fc 'setCourseGoals()'
     y le paso como arg una función anonima de donde obtenga su estado actual y luego retorna un estado actualizado */

     /*courseGoals=[...currentGoals,enteredGoal] -> extrae desde currentGoals,le mete enteredGoal y guarda en courseGoals,
      eso lo hace ()=>{[]}. courseGoals=()=>{[matriz actualizada]}, RN agiga esta tarea a flecha y continua hasta que le reporte */
    setCourseGoals(  // Defino una fc para que se encargue de la actualizacion de estado de objetivos.
      currentGoals =>
        [...currentGoals, { /* /currentGoals: lista de objetivos  del curso anteriores que ya existen;  ... extrae solo sus elementos de currentGoals
           Nuevo objeto js ingresado
            Modificación a la entrada, la convierto en objeto js y le agrego una clave
        key:Math.random().toString(), // La clave debe ser una cadena, la convierto con toString*/
            id: Math.random().toString(), /* CASO QUE NO TENGA KEY y quiera usar id como key
            value: enteredGoal  // nuevo objetivo ingresado desde TextInput que se agrega a la cima de la matriz currentGoals
          */
            value: goalTitle} /* El objetivo se agrega cuando precionamos boton y recivido desde GoalInput*/
        ]
    ); // Retorno mi matriz actualizada con currenGoals<
    setIsAddMode(false);
  };
    const removeGoalHandler = goalId =>{
      setCourseGoals(currentGoal=> {
        return currentGoals.filter((goal) => goal.id !== goalId);
      });
    };
  
    const cancelGoalAdditionHandler = () =>{
      setIsAddMode(false);
    };
  return (
    <View style={styles.screen}>
      <Button title="Add New Goal" onPress={()=>setIsAddMode(true)} />
    {/*
 
        <View style={styles.inputContainer}>

        <TextInput
                 // function se exe por cada tecla pusheded. si uso goalInputHandler() se exe
                 //antes de escribir el primer caracter, sin () espera a escribirlo o la 1° pulsación pa exe
        style={styles.input}
        placeholder='input'

        //Paso la fc goalInputHandler a onChangeText, esto le dirá a RN que por cada pulsaciṕn de teclado, exe esa función goalInputHandler 
        onChangeText={goalInputHandler} //LLamo a la constante goalInputHandler, que recibe una fc flecha

        //En cada pulsación de tecla, la variable enteredGoal mostrará su nuevo valor acá
        value={enteredGoal}  />

        <Button title="ADD" onPress ={addGoalHandler}></Button>
      </View>

    */}
       {/* <GoalInput title  ={goalInputHandler} entered={enteredGoal} addGoal={addGoalHandler}/> */}

       {/*onAddGoal que apunta a addGoalHandler, se recibe como una prop en el componente GoalInput ,
       e indica que esa prop se puede exe como una fc*/}
       {/*Le pago la función onAddGoal al componente hijo GoalInput como props */}
       <GoalInput visible={isAddMode} onAddGoal={addGoalHandler} onCancel={cancelGoalAdditionHandler } />
      {/*Para pocos elementos 20 o 30 la scrollView sirve sizeStatic, pero listas infinitas es preferible FlatList  */}
      <FlatList // FlatList Busca en el lemento una prop clave, o usa keyExtractor si no esta definida
      //Propiedad ocional si no tengo campo key en el objeto data, extraer clave
       keyExtractor={(item,index)=>item.id } // fc que indica a FlatList como extraer la calve.(elemento,suIndice) y retorna clave 
        data={courseGoals}      //=> item.key}  , FlatList le avisa a RN como obtener la key unica pa cada elemenyo
          renderItem =
            {
              itemData =>(
            /* data será el array fuente o entradas  de objetivos y renderItem = fc que se llama pa cada uno de estos datos de representación  */
              /*(
               <View  style={styles.listItem}>
                  <Text>{itemData.item.value}</Text>
              </View>
              )*/
              //itemData : el volumen de datos, item: cada elemento del volumen y value: valor deseado
              <GoalItem
              id={itemData.item.id}
              onDelete={removeGoalHandler}
              title={itemData.item.value}
              />
              )} /* renderItem = fc que retorna un componente por cada elemento de la matriz data = courseGoals */
      />

    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 30
  }
});


/*En realidad, RN ahora también admite "id" en lugar de "clave" (admite ambos).
 Simplemente intente "uid" y verá la advertencia.*/