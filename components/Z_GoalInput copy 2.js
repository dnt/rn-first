import React, {useState} from 'react';
import {View, TextInput,StyleSheet,Button,Modal} from 'react-native';

// Es un componente tipo funcional
const GoalInput =(props) =>{

 /*enteredGoal toma entrada desde TextInput y setEnteredGoal la modifica*/
  const [enteredGoal, setEnteredGoal] = useState('');
  
  const goalInputHandler=(enteredText)=>{
    setEnteredGoal(enteredText);
  };

const addGoalHandler=()=>{
  //props.onAddGoal.bind(this,enteredGoal) ;
  props.onAddGoal(enteredGoal);
  setEnteredGoal('');
}

  return(
    <Modal visible={props.visible} animationType="slide">
        <View style={styles.inputContainer}>
            {    /*function se exe por cada tecla pusheded. si uso goalInputHandler() se exe
            antes de escribir el primer caracter, sin () espera a escribirlo o la 1° pulsación pa exe */ }
            <TextInput
            style={styles.input}
            placeholder='input'

            /*Paso la fc goalInputHandler a onChangeText, esto le dirá a RN que por cada pulsaciṕn de teclado, exe esa función goalInputHandler */
            //onChangeText={goalInputHandler} //LLamo a la constante goalInputHandler, que recibe una fc flecha
            onChangeText={goalInputHandler}
            /*En cada pulsación de tecla, la variable enteredGoal mostrará su nuevo valor acá */
            //value={enteredGoal}  
            value={enteredGoal} />

            {/* <Button title="ADD" onPress ={addGoalHandler}></Button> */
            /* <Button title="ADD" onPress ={props.onAddGoal}></Button> */
            /* <Button title="ADD" onPress ={ ()=> props.onAddGoal(enteredGoal) }></Button> */
            /* desde este componente activaré la fc addGoalHandler que permanecerá en App; en cada presionar*/}
            <View style={styles.buttonContainer}>
             <View style={styles.button}>
                <Button title="Cancel" color="red" onPress = {props.onCancel} />
             </View>  
              <View style={styles.button}>
                <Button title="ADD" onPress ={addGoalHandler}/> 
              </View>
            
            </View>
            {/* addGoalHandler es el unico que puede administrar la lista ya que solo App tiene acceso a enteredGolal 
            y FlatList . This toma el ambiente del objeto que lo instancie*/}

        </View>
        </Modal>
    );
        /***bind(this,enteredGoal); this es una restricción de javascript, debido a que el parámetro 
         * que pasas al event handler onPress  puede venir desde cualquier fuente y no necesariamente 
         * desde el componente actual.***/
};
const styles = StyleSheet.create({
    inputContainer: {
        //flexDirection: 'row',
        //flexDirection: 'column', // x default
        //justifyContent: 'space-between',
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
      },
      input: {
        width: '80%',
        borderColor: 'red',
        borderWidth: 1,
        padding: 10,
        marginBottom:10
      },
      buttonContainer:{
        flexDirection:'row',
        justifyContent:'space-around',
        width:'60%'
      }, 
      button:{
        width:'40%'
      }
})
export default GoalInput;