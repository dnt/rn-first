    /* ¿Cómo pasar datos entre el niño y el padre en react-native? */
    
    module.exports= class APP extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      key1:'key1',
      key2:'key2'
    };
  render() {
    return (
       <View>
          <Button
             onPress={this.goToTisch}>
             1
          </Button>
          <Button
             onPress={this.goToTisch}>
             2
          </Button>
       </View>
    );
  }
}

/*SOLUCION:
Para llamar al método Parent desde child, puede pasar la referencia de esta manera.

Clase de padres:
<ChildClass
    onRef={ref => (this.parentReference = ref)}
    parentReference = {this.parentMethod.bind(this)}
/>

parentMethod(data) { }

Clase hijo:
let data = {
    id: 'xyz',
    name: 'zzz',
};

// Esto llamará al método de padre_
this.props.parentReference(data);


TAMBIEN:

Pasando argumentos a escuchadores de eventos

Dentro de un bucle es muy común querer pasar un parámetro extra a un manejador de eventos. Por 
ejemplo, si id es el ID de una fila, cualquiera de los códigos a continuación podría funcionar:

<button onClick={(e) => this.deleteRow(id, e)}>Delete Row</button>
<button onClick={this.deleteRow.bind(this, id)}>Delete Row</button>


Las dos líneas anteriores son equivalentes, y utilizan funciones flecha y Function.prototype.bind respectivamente.

En ambos casos, el argumento e que representa el evento de React va a ser pasado como un segundo argumento después 
del ID. Con una función flecha, tenemos que pasarlo explícitamente, pero con bind cualquier argumento adicional 
es pasado automáticamente Edita esta página

>>>> https://www.oscarblancarteblog.com/2018/10/04/react-props-propiedades/
   Las propiedades son la forma que tiene React para pasar parámetros de un componente padre a los hijos.normal que
  un componente pase datos a los componentes hijos, sin embargo, no es lo único que se puede pasar, si no que existe
  ocasiones en las que los padres mandar funciones a los hijos, para que estos ejecuten operaciones de los padres,
   puede sonar extraño, pero ya veremos cómo funciona.


*/
