import React from 'react';
import {View,Text,StyleSheet, TouchableOpacity} from 'react-native';

const GoalItem = props =>{
    //retorno JSX para reenderizar, este usa componentes RN u otros personalizados que usan componentes RN
        return(
        /* data será el array fuente o entradas  de objetivos y renderItem = fc que se llama pa cada uno de estos datos de representación  */
        <TouchableOpacity activeOpacity={0.8} onPress={props.onDelete.bind(this,props.id)}>
            <View  style={styles.listItem}>
                {/* <Text>{itemData.item.value}</Text> */}
                <Text>{props.title}</Text>
            </View>
        </TouchableOpacity>
        /* renderItem = fc que retorna un componente por cada elemento de la matriz data = courseGoals */
        );

};
const styles = StyleSheet.create({
    listItem:{
        padding:10,
        marginVertical:10,
        backgroundColor:'#ccc',
        borderColor:'black',
        borderWidth:1
      }
})
export default GoalItem;